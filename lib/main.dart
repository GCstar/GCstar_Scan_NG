import 'package:flutter/material.dart';
import 'Screens/ConnectScreen.dart';

void main() {
  runApp(GCstarScanner());
}

class GCstarScanner extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'GCstar Scanner',
      home: ConnectScreen(),
    );
  }
}
