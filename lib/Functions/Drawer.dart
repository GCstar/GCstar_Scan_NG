import 'package:flutter/material.dart';

import '../Screens/HistoryScreen.dart';
import '../Screens/ScannerScreen.dart';
import '../Screens/ConnectScreen.dart';
import 'Utils.dart' as utils;

class MyDrawer extends StatelessWidget {
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        decoration: BoxDecoration(
          color: Colors.black12,
        ),
        child: ListView(
          children: <Widget>[
            DrawerHeader(
              child: Center(
                child: CircleAvatar(
                  backgroundImage: AssetImage("assets/gcstar_scanner_256x256.png"),
                  radius: 75.0,
                ),
              ),
            ),
            Divider(color: Colors.black38),
            Text(
              "  IP : " + utils.gip,
              style: TextStyle(fontSize: 18),
            ),
            Text(
              "  PORT : " + utils.gport.toString(),
              style: TextStyle(fontSize: 18),
            ),
            Divider(color: Colors.black38),
            Container(
              color: Colors.white,
              child: Column(
                children: [
                  ListTile(
                    onTap: () => Navigator.push(context,
                        MaterialPageRoute(builder: (context) => ScannerScreen())),
                    title: Text(
                      "Scanner",
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                  Divider(
                    color: Colors.black38,
                  ),
                  ListTile(
                    title: Text(
                      "History",
                      style: TextStyle(fontSize: 18),
                    ),
                    onTap: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => HistoryScreen())),
                  ),
                  Divider(
                    color: Colors.black38,
                  ),
                  ListTile(
                      title: Text(
                        "Connect",
                        style: TextStyle(fontSize: 18),
                      ),
                      onTap: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ConnectScreen()))),
                  Divider(
                    color: Colors.black38,
                  ),
                  ListTile(
                    title: Text(
                      "About",
                      style: TextStyle(fontSize: 18),
                    ),
                    onTap: () => showAboutDialog(
                        context: context,
                        applicationName: 'GCstar Scan NG',
                        applicationVersion: '1.0.1',
                        applicationLegalese: utils.aboutText
                    ),
                  ),
                  Divider(
                    color: Colors.black38,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
