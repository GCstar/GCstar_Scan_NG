library gcstarscanner.utils;

import 'dart:io';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:package_info_plus/package_info_plus.dart';

const beepPath = 'beep.mp3';
const beepEmptyPath = 'beep_empty.mp3';

String gip = "";
int gport = 0;
var gsocket;

String glocation = "";
String gtag = "";
String gformat = "";
String gcontent = "";

bool gconnect = false;  // is the socket connected to GCstar?
bool listening = false; // is the app waiting for a response?
bool scanning = true;   // is the app scanning (not resending history)?

List<String> ghistory = [];
List<String> ghistoryLocations = [];
List<String> ghistoryTags = [];
final ghistorylength = ValueNotifier<int>(0);

Future<SharedPreferences> lgprefs = SharedPreferences.getInstance();
var currentContext;     // Flutter context to access the UI
final titleresp = ValueNotifier<String>("");  // to display the item title
final player = new AudioPlayer();
final gaddress = ValueNotifier<String>("");

String versionApp = "";
String buildApp = "";

var aboutText = "";

void init() {
  PackageInfo.fromPlatform().then((PackageInfo packageInfo) {
    versionApp = packageInfo.version;
    buildApp = packageInfo.buildNumber;

    aboutText = "Application to scan barcodes and send them to the collection manager GCstar.\n\n";
    aboutText += "Initial development: Léonard Lake\n\n";
    aboutText += "Maintenance: Kerenoc\n\n";
    aboutText += "License: GPL v3\n\n";
    aboutText += "Site: gitlab.com/GCstar/GCstar_Scan_NG\n\n";
    aboutText += "Version: "+versionApp+":"+buildApp+"\n\n";
    aboutText += "Usage:\n";
    aboutText += " - enter the IP address and port used by GCstar [Connect]\n";
    aboutText += " - if necessary, enter a location and tags for the items [Scanner]\n";
    aboutText += " - scan items\n";
    aboutText += " - for items without barcode, enter an ISBN/EAN number and press Scan\n";
    aboutText += " - when GCstar is not connected, codes go in history\n";
    aboutText += " - when GCstar is connected, sync the history to upload items [History]\n";
  });
}

void message(context, String text, {bool long = false}) {
  int duration = 100;
  // long messages for important information
  if (long) {
    duration = 2000;
  }
  ScaffoldMessenger.of(context).hideCurrentSnackBar();
  ScaffoldMessenger.of(context)
      .showSnackBar(SnackBar(content: Text(text), duration: Duration(milliseconds: duration)));
}

void sendResult(context, String content, String format, String location, String tag, [bool fromScanner = true]) async {
  scanning = fromScanner;
  currentContext = context;
  String xml = "<scans><scan format= '" +
      format +
      "'>" +
      content +
      "</scan><location>" +
      location +
      "</location><tags>" +
      tag +
      "</tags></scans>\n";
  if (gsocket == null || gconnect == false) {
    try {
      if (gsocket != null) {
        gsocket.close();
        print("closing");
        gconnect = false;
      }
      gsocket = await Socket.connect(gip, gport,
          timeout: Duration(milliseconds: 1000));
      gconnect = true;
      listening = false;
      message(context, "Connection to GCstar succeeded");
    } catch (e) {
      gconnect = false;
      message(context, "Connection failed", long: true);
      print(e);
    }
  }
  try {
    if (gconnect == true) {
      gsocket.write(xml);
      await gsocket.flush();

      if (listening == false) {
        listening = true;
        gsocket.listen(getResponse,
            onError: ((error, StackTrace trace) {
              print("!!!!! Listen error $error $trace");
              message(context, "Error on connexion : $error", long: true);
            }),
            onDone: (() {
              print("!!!!! Server stopped");
              message(context, "GCstar disconnected", long: true);
              listening = false;
              gsocket.close();
              gsocket.destroy();
            }),
            cancelOnError: false);
      }
    } else if (scanning) {
      ghistory.add(gcontent);
      ghistoryLocations.add(glocation);
      ghistoryTags.add(gtag);
      ghistorylength.value = ghistory.length;
      await saveHistory();
      message(context, "Recorded in history (offline)", long: true);
    }
  } catch (e) {
    gconnect = false;
    print("Error : $e");
  }
}

void resendHistory (context) async {
  if (ghistory.length > 0) {
    var item = ghistory.last;
    var location = ghistoryLocations.last;
    var tag = ghistoryTags.last;
    print('Resending ' + item);
    sendResult(context, item, "EAN-13", location, tag, false);
  } else  {
    await saveHistory();
  }
}

void getResponse(data) {
  final List<int> byteArray = data;

  String title = String.fromCharCodes(byteArray).trim();
  print('Received ' + title);
  if (title =='_STOP_') {
    print("!!!!! GCstar finished scanning");
    message(currentContext, "GCstar stopped scanning", long: true);
    listening = false;
    gsocket.close();
    gsocket.destroy();
  }
  RegExp regex = RegExp("<result><title>(.*?)</title></result>");
  titleresp.value = regex.firstMatch(title)?.group(1) ?? "";
  print("titlerecp : " + titleresp.value);
  if (titleresp.value == '') {
    player.play(beepEmptyPath);
  } else {
    player.play(beepPath);
  }
  if (!scanning) {
    message(currentContext, titleresp.value, long: true);
    print('Next history item ${ghistory.length}');
    ghistory.removeLast();
    ghistoryLocations.removeLast();
    ghistoryTags.removeLast();
    ghistorylength.value = ghistory.length;
    resendHistory(currentContext);
  }
}

void clearHistory() {
  ghistory = [];
  ghistoryLocations = [];
  ghistoryTags = [];
  ghistorylength.value = 0;
  saveHistory();
}

Future<void> saveHistory () async {
  final SharedPreferences prefs = await lgprefs;
  await prefs
      .setStringList('history', ghistory)
      .then((bool success) {});
  await prefs
      .setStringList('historyLocations', ghistoryLocations)
      .then((bool success) {});
  await prefs
      .setStringList('historyTags', ghistoryTags)
      .then((bool success) {});
}

Future<void> savePort () async {
  final SharedPreferences prefs = await lgprefs;
  gaddress.value = gip + " / " + gport.toString();
  await prefs
      .setInt('port', gport)
      .then((bool success) {});
}

Future<void> saveIP () async {
  final SharedPreferences prefs = await lgprefs;
  gaddress.value = gip + " / " + gport.toString();
  await prefs
      .setString('ip', gip)
      .then((bool success) {});
}

Future<void> getInfos() async {
  SharedPreferences prefs = await lgprefs;
  gip = (prefs.getString('ip') ?? "192.168.1.10");
  gport = (prefs.getInt('port') ?? 50007);
  gaddress.value = gip + " / " + gport.toString();

  ghistory = (prefs.getStringList('history') ?? []);
  ghistoryLocations = (prefs.getStringList('historyLocations') ?? []);
  ghistoryTags = (prefs.getStringList('historyTags') ?? []);
  ghistorylength.value = ghistory.length;
}