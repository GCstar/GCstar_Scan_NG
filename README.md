# GCstar_Scan_NG

Portable app to scan items (books, gadgets, DVD, CD...) for the collection manager **GCstar** (https://gitlab.com/GCstar/GCstar).
This app may replace the existing app for Android GCstar Scanner that was not published under an open source license.

It adds some new functionalities:

- feedback (title and beep) when an item is found by GCstar
- history of items scanned in offline mode, to be synched later with GCstar 
- possibility to add a location and tags (semicolon separated list) to scanned items
- possibility to enter manually an IBAN/ISBN code if no barcode is on an item

## Usage

Launch **GCstar** and choose "File/Import/Scan barcode" function. Select "Network scanning" then "Import"

Launch **GCstar_Scan_NG**, in the **Connect** screen, verify that the IP address and port match the address and port displayed on GCstar scan dialog and select "Connect". Scan a barcode from the **Scanner** screen : GCstar will search the article on the web or in the current collection and display its title in the scan dialog when found. GCstar_Scan_NG will display the title and emit a beep. If the item is not found, another type of beep will be emitted.

If GCstar is not executing and GCstar_Snc_NG can't connect to it, the scanned barcodes will be inserted in the History. From the **History** screen, once GCstar is connected again, the "Sync" button will send the barcodes to GCstar for insertion in the current collection.

## Installation for developpers ##
To install flutter, use this link : (https://flutter.dev/docs/get-started/install)
Then clone this project from Gitlab and create a flutter project (flutter create .)
Install dependencies (flutter pub get)
Generate launcher icon (flutter pub run flutter_launcher_icons:main)

```
git clone https://gitlab.com/GCstar/GCstar_Scan_NG.git
mv GCstar_Scan_NG scan_ng
cd scan_ng
flutter create --org com.gcstar scan_ng
flutter pub get
flutter pub run flutter_launcher_icons:main
flutter run
```

Before building an APK for release, insert the following line in **Android/app/src/main/AndroidManifest.xml**

```
    <uses-permission android:name="android.permission.INTERNET"/>
```

## Android Device ##
Tested on Android

## IOS Device ##
Not tested yet
